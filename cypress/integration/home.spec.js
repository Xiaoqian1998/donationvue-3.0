// eslint-disable-next-line no-undef
context('Querying', () => {
  // eslint-disable-next-line no-undef
  before(() => {
    // eslint-disable-next-line no-undef
    cy.visit('http://localhost:8080')
  })

  // eslint-disable-next-line no-undef
  it('Home Page', () => {
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-home').should('contain', 'Home')
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-list').should('contain', 'Manage Dishes')
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-money').should('contain', 'Comment')
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-money').should('contain', 'Comment List')
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-globe').should('contain', 'Map')
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-youtube-play').should('contain', 'Video')
    // eslint-disable-next-line no-undef
    cy.get('.hero .vue-title').should('contain', 'Homer for President !!')
  })
})
