// eslint-disable-next-line no-undef
context('Querying', () => {
  // eslint-disable-next-line no-undef
  before(() => {
    // eslint-disable-next-line no-undef
    cy.visit('http://localhost:8080/#/map')
  })

  // eslint-disable-next-line no-undef
  it('Map Page', () => {
    // eslint-disable-next-line no-undef
    cy.get('.vue-title').should('contain', 'Google Map')
    // eslint-disable-next-line no-undef
    cy.get('.map-view').should('be.visible')
    // eslint-disable-next-line no-undef
    cy.get('.map-view').should('has.css', 'overflow', 'hidden')
  })
})
