// eslint-disable-next-line no-undef
context('Querying', () => {
  // 只有一个测试(it)的时候可以用before,多个的时候用beforeEach
  // 每一行都有个eslint-disable-next-line no-undef 这个eslint检查代码用的不用管它，但是得加，否则无法运行
  // eslint-disable-next-line no-undef
  beforeEach(() => {
    // eslint-disable-next-line no-undef
    cy.visit('http://localhost:8080/#/dished')
  })

  // eslint-disable-next-line no-undef
  it('Make Comment 02', () => {
    // eslint-disable-next-line no-undef
    cy.get('.vue-title').should('contain', 'Make Comment')
    // eslint-disable-next-line no-undef
    cy.get('#code').select('02')
    // eslint-disable-next-line no-undef
    cy.get('#title').select('like')
    // eslint-disable-next-line no-undef
    cy.get('button.btn1').click().get('.error').should('be.visible')
    // eslint-disable-next-line no-undef
    cy.get('.form__input').invoke('val', 'Taste Good!')
    // eslint-disable-next-line no-undef
    cy.get('button[type=submit]').click()
    // eslint-disable-next-line no-undef
    cy.get('a.btn1').click()
  })
  // eslint-disable-next-line no-undef
  it('Make Comment 01', () => {
    // eslint-disable-next-line no-undef
    cy.get('#code').select('01')
    // eslint-disable-next-line no-undef
    cy.get('#title').select('dislike')
    // eslint-disable-next-line no-undef
    cy.get('button.btn1').click().get('.error').should('be.visible')
    // eslint-disable-next-line no-undef
    cy.get('.form__input').invoke('val', 'I don\'t like hamburger!')
    // eslint-disable-next-line no-undef
    cy.get('button[type=submit]').click()
    // eslint-disable-next-line no-undef
    cy.get('a.btn1').click()
  })
})
