// eslint-disable-next-line no-undef
context('Video', () => {
  // eslint-disable-next-line no-undef
  before(() => {
    // eslint-disable-next-line no-undef
    cy.visit('http://localhost:8080/#/video')
  })

  // eslint-disable-next-line no-undef
  it('Video Page', () => {
    // eslint-disable-next-line no-undef
    cy.get('.fa.fa-youtube-play').should('contain', 'Video')
    // eslint-disable-next-line no-undef
    cy.get('video').should('be.visible').should('have.attr', 'controls', 'controls')
  })
})
