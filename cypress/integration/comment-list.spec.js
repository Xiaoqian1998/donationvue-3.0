// eslint-disable-next-line no-undef
context('Querying', () => {
  // eslint-disable-next-line no-undef
  before(() => {
    // eslint-disable-next-line no-undef
    cy.visit('http://localhost:8080/#/comment')
  })

  // eslint-disable-next-line no-undef
  it('Comment List', () => {
    // eslint-disable-next-line no-undef
    cy.get('.vue-title').should('contain', 'Comment List')
    // eslint-disable-next-line no-undef
    cy.get('.VueTables__table').find('tr').should(($tr) => {
      // eslint-disable-next-line no-undef
      expect($tr).to.have.length(4) // 列表中有四行(表头+搜索框+数据)，数据行数+2
    })
  })
})
