import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: 'https://dishes-prod-api.herokuapp.com/'
    // baseURL: 'https://donationweb-ssd-nodeserver.herokuapp.com/'
  })
}
