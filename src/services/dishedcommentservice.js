import Api from '@/services/api'

export default {
  fetchComments () {
    return Api().get('/comments/')
  },
  postComment (comment) {
    return Api().post('/comments', comment,
      { headers: {'Content-type': 'application/json'} })
  },
  upvoteComment (id) {
    return Api().put(`/comment/${id}/vote`)
  },
  downvoteComment (id) {
    return Api().put(`/comment/${id}/dvote`)
  },
  deleteComment (id) {
    return Api().delete(`/comment/${id}`)
  },
  fetchComment (id) {
    return Api().get(`/comments/${id}`)
  },
  putComment (id, comment) {
    console.log('REQUESTING ' + comment._id + ' ' +
      JSON.stringify(comment, null, 5))
    return Api().put(`/comments/${id}`, comment,
      { headers: {'Content-type': 'application/json'} })
  }
}
