import Api from '@/services/api'

export default {
  postUser (user) {
    return Api().post('/user', user,
      { headers: {'Content-type': 'application/json'} })
  },
  fetchUser (username, password) {
    return Api().get(`/user/${username}/${password}`)
  }
}
