import Api from '@/services/api'

export default {
  fetchDishes () {
    return Api().get('/dishes')
  },
  postDished (dished) {
    return Api().post('/dishes', dished,
      { headers: {'Content-type': 'application/json'} })
  },
  upvoteDished (id) {
    return Api().put(`/dished/${id}/vote`)
  },
  downvoteDished (id) {
    return Api().put(`/dished/${id}/dvote`)
  },
  deleteDished (id) {
    return Api().delete(`/dished/${id}`)
  },
  fetchDished (id) {
    return Api().get(`/dishes/${id}`)
  },
  putDished (id, dished) {
    console.log('REQUESTING ' + dished._id + ' ' +
      JSON.stringify(dished, null, 5))
    return Api().put(`/dishes/${id}`, dished,
      { headers: {'Content-type': 'application/json'} })
  }
}
