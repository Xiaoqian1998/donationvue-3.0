import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Donations from '@/components/Donations'
import Donate from '@/components/Donate'
import AboutUs from '@/components/AboutUs'
import ContactUs from '@/components/ContactUs'
// import Edit from '@/components/Edit'
import Dishes from '@/components/Dishes'
import Dished from '@/components/Dished'
import DishesComment from '@/components/DishesComment'
import CommentEdit from '@/components/CommentEdit'
import Map from '@/components/Map'
import Login from '@/components/Login'
import Video from '@/components/Video'

import GoogleLogin from '@/components/GoogleLogin'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/donations',
      name: 'Donations',
      component: Donations
    },
    {
      path: '/dishes',
      name: 'Dishes',
      component: Dishes
    },
    {
      path: '/dished',
      name: 'Dished',
      component: Dished
    },
    {
      path: '/comment',
      name: 'DishesComment',
      component: DishesComment
    },
    {
      path: '/donate',
      name: 'Donate',
      component: Donate
    },
    {
      path: '/comment-edit',
      name: 'CommentEdit',
      component: CommentEdit,
      props: true
    },
    {
      path: '/about',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/contact',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/map',
      name: 'Map',
      component: Map
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/video',
      name: 'Video',
      component: Video
    },
    {
      path: '/gLogin',
      name: 'GoogleLogin',
      component: GoogleLogin
    }
  ]
})
